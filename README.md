# MSBuild Notes

### Going through the examples

Each header in this document has an associated tag in the repo in
in parenthesis type ```git checkout tagname``` to use and run the
code.  If you don't have git, you can download a zip file from the
tags section on
[github](https://github.com/MountainCode/msbuild_demo/tags).

### Running MSBuild (simple)

To run msbuild type

```bash
$ msbuild
```

### Targets (targets)

Define a target with the <Target> tag.

```xml
<Target Name="Build">
  ...
</Target>
```

MSBuild will run the first target by default.  A different target can
be chosen as an argument.

```bash
$ msbuild /t:clean
```

To run multiple targets, separate with semicolons.

```bash
$ msbuild /t:clean;build
```

The default target can be changed in the Project tag.

```xml
<Project ... DefaultTargets="Build">
```

Multiple targets can be defined here as well.

```xml
<Project ... DefaultTargets="Clean;Build">
```

### Property Groups (properties)

Property groups define variables to use elsewhere in the project file.
The syntax to reference a property is ```$(PropertyName)```.

```xml
<PropertyGroup>
  <AssemblyName>DemoProject</AssemblyName>
</PropertyGroup>
...
  <Message Text="Building $(AssemblyName)" />
```

### Item Groups (items)

Item groups define list variables.  All elements in an item group should
begin with the same tag name.  Each tag must have an "Include" attribute
which is its value.

```xml
  <ItemGroup>
    <Compile Include="ProductFamily.vb" />
    <Compile Include="PfMember.vb" />
  </ItemGroup>
  ...
    <Message Text="Compiling @(Compile)" />
```

### Compiling (compiling)

You can set the .NET version in the Project tag.

```xml
<Project ... ToolsVersion="4.0" />
```

Build C# files with the *Csc* task and VB files with the *Vbc* task.

```xml
<Vbc Sources="@(Compile)" />
```

If building a library, you must set the TargetType attribute.

```xml
<Vbc Sources="@(Compile)" TargetType="Library" />
```

#### Output Assembly (assembly)

Specify the output of a compile with the OutputAssembly attribute.

```xml
<PropertyGroup>
  <AssemblyName>BuildTask</AssemblyName>
  <Configuration>Debug</Configuration>
  <OutputPath>bin\$(Configuration)\</OutputPath>
...
<Vbc ... OutputAssembly="$(OutputPath)$(AssemblyName).dll" />
```

The *OutputPath* will need to be created before generating the 
*OutputAssembly*.

```xml
  <MakeDir Condition="!Exists('$(OutputPath)')"
    Directories="$(OutputPath)" />
```

### Cleaning (cleaning)

Files can be deleted with the Delete task and directories with the
RemoveDir task.

```xml
  <ItemGroup>
    <Delete Include="$(OutputPath)$(AssemblyName).dll" />
  </ItemGroup>
  <Target Name="Clean">
    <Delete Files="@(Delete)" />
    <RemoveDir Directories="$(OutputPath);bin" />
  </Target>
```

### Debug Files (debug)

Files can be debugged with the DebugType and EmitDebugInformation
attributes of the Vbc tag.

```xml
<Vbc Sources="@(Compile)" ...
     EmitDebugInformation="true"
     DebugType="pdbonly" />
```

### Configurations (config)

Configurations are defined as a property.

```xml

<PropertyGroup>
  ...
  <Configuration
    Condition="$(Configuration) == ''">Debug</Configuration>
```

Then other properties can be added depending on how the value of the
configuration variable.

```xml
<PropertyGroup Condition="$(Configuration) == 'Debug'">
  <DebugSymbols>true</DebugSymbols>
  <DebugType>pdbonly</DebugType>
</PropertyGroup>
<PropertyGroup Condition="$(Configuration) == 'Release'">
  <DebugSymbols>false</DebugSymbols>
  <DebugType />
</PropertyGroup>
```

Then the properties can be used in the Vbc task.

```xml
<Vbc Sources="@(Compile)"
  EmitDebugInformation="$(DebugSymbols)"
  DebugType="$(DebugType)" />
```

Specify the configuration property as an argument to msbuild.

```bash
$ msbuild /p:Configuration=Release
$ msbuild /p:Configuration=Release /t:clean
```

### Incremental Builds (builds)

A task can specify *Inputs* and *Outputs* attributes.  If the
modification timestamp of all input files are earlier than modification
timestamp of all output files, the task will be skipped.

```xml
<ItemGroup>
  <BuildOutput Include="$(OutputPath)$(AssemblyName).dll" />
</ItemGroup>
<ItemGroup Condition="$(Configuration) == 'Debug'">
  <BuildOutput Include="$(OutputPath)$(AssemblyName).pdb" />
</ItemGroup>
<Target Name="Build" Inputs="@(Compile)" Outputs="@(BuildOutput)">
```

It is important to understand that this is based of the file timestamp
and not the contents of the files.  If a file with an earlier timestamp
is copied over an input file, the task will be skipped even though the
inputs have changed.

```bash
$ cat << 'EOF' >> PfMember.vb
Enum Availability
  InStock
  OutOfStock
End Enum
EOF
```

```
$ touch -d '2 hours ago' PfMember.vb
```

### Task Dependencies (deps)

Task dependencies can be defined for target.  The dependency tasks will
run first and a task will not run if a dependency fails.

```xml
  <Target Name="Rebuild" DependsOnTargets="Clean;Build">
    <Message Text="Rebuild Complete" />
  </Target>
```

You can also specify multiple tasks as *msbuild* arguments and they will
be run in order.

```bash
$ msbuild "/t:clean;build"
```
